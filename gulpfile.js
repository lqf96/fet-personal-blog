"use strict";

var gulp = require("gulp"),
    minifycss = require("gulp-minify-css"),
    uglify = require("gulp-uglify"),
    rename = require("gulp-rename"),
    jshint = require("gulp-jshint");

//Javascript grammer check
gulp.task("jshint", function()
{   return gulp.src("assets/*.js")
        .pipe(jshint())
        .pipe(jshint.reporter("default"));
});

//Minify CSS
gulp.task("minifycss", function()
{   return gulp.src("assets/*.css")
        .pipe(rename({suffix: ".min"}))
        .pipe(minifycss())
        .pipe(gulp.dest("assets-final"));
});

//Minify javascript
gulp.task("minifyjs", function()
{   return gulp.src("assets/*.js")
        .pipe(rename({suffix: ".min"}))
        .pipe(uglify())
        .pipe(gulp.dest("assets-final"));
});

//Copy other files
gulp.task("copyfiles", function()
{   return gulp.src([
        "assets/*.jpeg",
        "assets/*.json",
        "assets/*.woff"
    ]).pipe(gulp.dest("assets-final"));
})

//Default task
gulp.task("default", ["jshint"], function()
{   gulp.start("minifycss", "minifyjs", "copyfiles");
});
