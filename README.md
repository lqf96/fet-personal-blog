# [FET Personal Blog](https://lqf96.coding.me/fet-personal-blog/)
Personal blog exercise for FET course.

## Assignment Requirements
* Mandatory
  - [Personal introduction](https://lqf96.coding.me/fet-personal-blog/)
  - [Demos & Web Frontend Homeworks](https://lqf96.coding.me/fet-personal-blog/#/demos)
  - [Blogs](https://lqf96.coding.me/fet-personal-blog/#/blogs)
  - Support mainstream browsers
  - Publish on Github Pages
  - Write documentation about your own website
* Bonus
  - Implement responsive design
  - Use Gulp to compress assets

## Homework Descriptions
* Personal website link: https://lqf96.coding.me/fet-personal-blog
* Details:
  - Responsive design
    + Container-based and grid-based design, both of which are inspired by Bootstrap, are widely used in this website.
    + Navigation bar uses another approach: they are inline block elements for tablets and computers, but are block elements for mobile phones.
  - Single Page Application (SPA) / Frontend routing
    + This site is single page application and all contents are loaded using AJAX and XMLHttpRequest.
    + No 3rd-party libraries (like jQuery) are used.
    + Javascript also handles the active status of the navigation bar.
  - Automated resource compression with Gulp
    + Before using gulp you must run `npm install -g gulp` and `npm install`.
    + Javascript code are first checked with JSHint and compressed with UglifyJS.
    + CSS code are directly compressed with MinifyCSS.
    + All compressed resources are stored in `assets-final` folder.
  - Browser compatability
    + Test with Safari and Chrome both on computer and iPhone. No problems.

## Course Suggestions
See [here](https://lqf96.coding.me/fet-personal-blog/#/blogs).

## License
[GNU GPLv3](LICENSE)
