"use strict";

//Run after resources fully loaded
window.addEventListener("load", function()
{   //Http451 namespace
    var http451 = window.http451 = {};

    //Load routes using XHR
    var routes_req = new XMLHttpRequest();
    routes_req.addEventListener("load", function()
    {   http451.routes = JSON.parse(routes_req.responseText);
        //Do SPA navigation
        spa_navigate();
    });
    routes_req.open("GET", "assets/routes.json");
    routes_req.send();
    //SPA navigation
    function spa_navigate()
    {   //View URL
        var v_url = location.hash?location.hash.substr(1):"/";
        var view_url = (v_url in http451.routes)?(http451.routes[v_url]+".html"):(v_url+".html");
        //Load view
        var view_req = new XMLHttpRequest();
        view_req.addEventListener("load", function()
        {   var view_area = document.getElementById("view-area");
            view_area.innerHTML = view_req.responseText;
        });
        view_req.open("GET", "views"+view_url);
        view_req.send();

        //Navigation bar elements
        var nav_items = document.getElementsByClassName("nav-item");
        for (var i=0;i<nav_items.length;i++)
        {   var item = nav_items[i];
            item.classList.remove("active");
            if (item.getAttribute("href")=="#"+v_url)
                item.classList.add("active");
        }
    }
    window.addEventListener("hashchange", spa_navigate);
});
